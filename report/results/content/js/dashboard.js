/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8648648648648649, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.25, 500, 1500, "http://localhost:8080/login/index.php"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-11"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-10"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/index.php-9"], "isController": false}, {"data": [0.75, 500, 1500, "http://localhost:8080/login/index.php-6"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/index.php-5"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/index.php-8"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/index.php-7"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/index.php-2"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-6"], "isController": false}, {"data": [0.75, 500, 1500, "http://localhost:8080/login/index.php-1"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/index.php-10"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-5"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/index.php-4"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-8"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/index.php-3"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-7"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-9"], "isController": false}, {"data": [0.75, 500, 1500, "http://localhost:8080/login/index.php-0"], "isController": false}, {"data": [0.5, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-0"], "isController": false}, {"data": [0.5, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-2"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-1"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-4"], "isController": false}, {"data": [1.0, 500, 1500, "http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-3"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 36, 0, 0.0, 265.9722222222223, 6, 2040, 59.5, 1084.0000000000002, 1431.399999999999, 2040.0, 4.015616285554936, 276.2540086447295, 4.607109244283324], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["http://localhost:8080/login/index.php", 2, 0, 0.0, 1553.0, 1066, 2040, 1553.0, 2040.0, 2040.0, 2040.0, 0.3755163349605708, 226.31881776896358, 2.4668929543747655], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-11", 1, 0, 0.0, 10.0, 10, 10, 10.0, 10.0, 10.0, 10.0, 100.0, 29.98046875, 66.796875], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-10", 1, 0, 0.0, 129.0, 129, 129, 129.0, 129.0, 129.0, 129.0, 7.751937984496124, 2.0515382751937983, 4.784399224806202], "isController": false}, {"data": ["Test", 1, 0, 0.0, 4232.0, 4232, 4232, 4232.0, 4232.0, 4232.0, 4232.0, 0.23629489603024575, 292.606000413516, 4.879812662452741], "isController": true}, {"data": ["http://localhost:8080/login/index.php-9", 2, 0, 0.0, 74.0, 19, 129, 74.0, 129.0, 129.0, 129.0, 0.5140066820868672, 4.66621691081984, 0.3152306604985865], "isController": false}, {"data": ["http://localhost:8080/login/index.php-6", 2, 0, 0.0, 332.5, 15, 650, 332.5, 650.0, 650.0, 650.0, 0.5243838489774515, 72.66966685238594, 0.3338850288411117], "isController": false}, {"data": ["http://localhost:8080/login/index.php-5", 2, 0, 0.0, 19.5, 15, 24, 19.5, 24.0, 24.0, 24.0, 0.5243838489774515, 4.709212768746722, 0.33567735644992136], "isController": false}, {"data": ["http://localhost:8080/login/index.php-8", 2, 0, 0.0, 139.5, 10, 269, 139.5, 269.0, 269.0, 269.0, 0.5285412262156448, 1.3590322740486258, 0.33833864627378435], "isController": false}, {"data": ["http://localhost:8080/login/index.php-7", 2, 0, 0.0, 12.5, 7, 18, 12.5, 18.0, 18.0, 18.0, 0.5291005291005292, 5.750093005952381, 0.3366298776455027], "isController": false}, {"data": ["http://localhost:8080/login/index.php-2", 2, 0, 0.0, 132.0, 92, 172, 132.0, 172.0, 172.0, 172.0, 0.5137426149499101, 1.1265713299511946, 0.3298689153609042], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-6", 1, 0, 0.0, 12.0, 12, 12, 12.0, 12.0, 12.0, 12.0, 83.33333333333333, 24.983723958333332, 56.15234375], "isController": false}, {"data": ["http://localhost:8080/login/index.php-1", 2, 0, 0.0, 434.0, 233, 635, 434.0, 635.0, 635.0, 635.0, 0.5270092226613965, 21.929811017786562, 0.2992733036890646], "isController": false}, {"data": ["http://localhost:8080/login/index.php-10", 1, 0, 0.0, 13.0, 13, 13, 13.0, 13.0, 13.0, 13.0, 76.92307692307693, 23.06189903846154, 55.06310096153847], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-5", 1, 0, 0.0, 17.0, 17, 17, 17.0, 17.0, 17.0, 17.0, 58.8235294117647, 17.635569852941174, 38.3157169117647], "isController": false}, {"data": ["http://localhost:8080/login/index.php-4", 2, 0, 0.0, 25.0, 19, 31, 25.0, 31.0, 31.0, 31.0, 0.5238344683080147, 25.63770707831325, 0.33174477802514407], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-8", 1, 0, 0.0, 6.0, 6, 6, 6.0, 6.0, 6.0, 6.0, 166.66666666666666, 50.130208333333336, 111.16536458333333], "isController": false}, {"data": ["http://localhost:8080/login/index.php-3", 2, 0, 0.0, 53.0, 6, 100, 53.0, 100.0, 100.0, 100.0, 0.5253480430785396, 171.02823540517468, 0.33116422051484107], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-7", 1, 0, 0.0, 11.0, 11, 11, 11.0, 11.0, 11.0, 11.0, 90.9090909090909, 27.254971590909093, 60.45809659090909], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-9", 1, 0, 0.0, 11.0, 11, 11, 11.0, 11.0, 11.0, 11.0, 90.9090909090909, 27.254971590909093, 60.36931818181819], "isController": false}, {"data": ["http://localhost:8080/login/index.php-0", 2, 0, 0.0, 806.0, 288, 1324, 806.0, 1324.0, 1324.0, 1324.0, 0.43975373790677225, 6.226552193271767, 0.2512265006596306], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t", 1, 0, 0.0, 1126.0, 1126, 1126, 1126.0, 1126.0, 1126.0, 1126.0, 0.8880994671403197, 29.249174345026645, 6.672020703818828], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-0", 1, 0, 0.0, 185.0, 185, 185, 185.0, 185.0, 185.0, 185.0, 5.405405405405405, 10.805532094594595, 2.9138513513513513], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-2", 1, 0, 0.0, 671.0, 671, 671, 671.0, 671.0, 671.0, 671.0, 1.4903129657228018, 39.17893070044709, 0.7742641579731743], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-1", 1, 0, 0.0, 128.0, 128, 128, 128.0, 128.0, 128.0, 128.0, 7.8125, 15.37322998046875, 3.94439697265625], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-4", 1, 0, 0.0, 6.0, 6, 6, 6.0, 6.0, 6.0, 6.0, 166.66666666666666, 50.130208333333336, 111.328125], "isController": false}, {"data": ["http://localhost:8080/login/logout.php?sesskey=3Aa60is37t-3", 1, 0, 0.0, 88.0, 88, 88, 88.0, 88.0, 88.0, 88.0, 11.363636363636363, 3.5622336647727275, 7.668235085227273], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 36, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
